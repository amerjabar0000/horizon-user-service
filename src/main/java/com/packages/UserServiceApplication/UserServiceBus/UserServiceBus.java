package com.packages.UserServiceApplication.UserServiceBus;

import com.rabbitmq.client.DeliverCallback;
import com.rabbitmq.client.Delivery;
import java.io.IOException;
import org.springframework.stereotype.Service;
import static com.packages.UserServiceApplication.UserServiceBus.Constants.*;

@Service
public class UserServiceBus {
    
    private final MessagePublisher publisher;
    private final MessageConsumer consumer;
    private final Actions actions;
    
    DeliverCallback callback = new DeliverCallback() {
        @Override
        public void handle(String string, Delivery dlvr) throws IOException {
            String message = new String(dlvr.getBody(), "UTF-8");
            System.out.println(message);
        }
    };
    
    public UserServiceBus(MessagePublisher publisher, MessageConsumer consumer, Actions actions) {
        this.publisher = publisher;
        this.consumer = consumer;
        this.actions = actions;
        
        this.publisher.initialize();
//        this.consumer.initialize();
//        this.consumer.startConsuming(QUEUE, callback);
    }
    
    public void userDeleted(Long id) {
        publisher.sendMessage(actions.deleteUserToAuth(id));
    }
    
}
