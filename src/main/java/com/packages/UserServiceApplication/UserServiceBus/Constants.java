package com.packages.UserServiceApplication.UserServiceBus;

public class Constants {
    
    public static final String HOSTNAME = "localhost";
    public static final String EXCHANGE_NAME = "exchange.from.user";
    public static final String EXCHANGE_TYPE = "topic";
    public static final String ROUTING_KEY = "user.to.auth";
    public static final String QUEUE = "auth.queue";
    
}
