package com.packages.UserServiceApplication.UserServiceBus;

import net.minidev.json.JSONObject;
import com.packages.UserServiceApplication.UserService.User;
import org.springframework.stereotype.Component;

@Component
public class Actions {
    
    public JSONObject deleteUserToAuth(Long id) {
        JSONObject message = new JSONObject();
        
        JSONObject payload = new JSONObject();
        payload.put("id", id);
        
        message.put("payload", payload);
        message.put("issuer", "User");
        message.put("issued to", "Auth");
        
        return message;
    }
    
    public JSONObject updateUserToAuth(User user) {
        JSONObject object = new JSONObject();
        object.put("id", user.getId());
        object.put("username", user.getUsername());
        object.put("firstName", user.getFirstName());
        object.put("lastName", user.getLastName());
        object.put("email", user.getEmail());
        object.put("password", user.getPassword());
        object.put("phoneNumber", user.getPhoneNumber());
        object.put("country", user.getCountry());
        object.put("age", user.getAge());
        
        JSONObject message = new JSONObject();
        
        message.put("issuer", "User");
        message.put("issued to", "Auth");
        message.put("payload", object);
        
        return message;
    }
    
}
