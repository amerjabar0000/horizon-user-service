package com.packages.UserServiceApplication.AuthMailService;

import org.simplejavamail.api.mailer.Mailer;
import org.simplejavamail.mailer.MailerBuilder;
import org.springframework.stereotype.Service;
import static com.packages.UserServiceApplication.AuthMailService.Constants.*;
import org.simplejavamail.api.email.Email;
import org.simplejavamail.email.EmailBuilder;

@Service
public class AuthMailService {
    
    private final Mailer inhouseMailer;
    
    public AuthMailService() {
        this.inhouseMailer = MailerBuilder
            .withSMTPServer(
                SMTP_SERVER_LINK,
                SMTP_SERVER_PORT,
                SMTP_SERVER_NAME,
                SMTP_SERVER_API_KEY
            )
            .buildMailer();
    }
    
    public void sendEmail(String recipient, String subject, String message) {
        Email email = new Email(
            EmailBuilder
                .startingBlank()
                .from(SENDER_EMAIL)
                .to(recipient)
                .withSubject(subject)
                .withPlainText(message)
        );
        
        this.inhouseMailer.sendMail(email);
    }
    
    public void sendForgetPasswordEmail(String recipient, String recipientEmail, String subject, String passwordResetToken) {
        Email email = new Email(
            EmailBuilder
                .startingBlank()
                .from(SENDER_EMAIL)
                .to(recipientEmail)
                .withSubject(subject)
                .withPlainText(forgottenPasswordEmailText(recipient, passwordResetToken))
        );
        
        this.inhouseMailer.sendMail(email);
    }
    
}
