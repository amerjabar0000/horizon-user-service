package com.packages.UserServiceApplication.UserService;

import com.packages.UserServiceApplication.AuthDetailService.AuthDetailService;
import static com.packages.UserServiceApplication.AuthDetailService.Constants.*;
import com.packages.UserServiceApplication.AuthDetailService.JWTTokenUtil;
import com.packages.UserServiceApplication.AuthMailService.AuthMailService;
import java.util.List;
import java.util.UUID;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@RestController
@RequestMapping("/api")
public class UserController {
    
    private final UserDetailService userDetailService;
    private final AuthDetailService authDetailService;
    private final AuthMailService authMailService;
    private final JWTTokenUtil jwtTokenUtil;
    
//    Huge change in plan. Merging Auth and User services and devoting Auth service
//    only for service authorization.
    
//    Requirements for User service:
//    1.User needs to login. Done
//    2.User needs to signup. Done
//    3.User needs to see his/her account. Done.
//    4.User needs to edit his/her account. Done.
//    5.User needs to delete his/her account.
//    6.We need to calculate user metrics.
    
//    Service bus should be enabled at the lastest stages of development
//    when most of the services are ready to be consumed
//    private final UserServiceBus userServiceBus;
    
    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedOrigins("*")
                        .allowedMethods("GET", "POST", "PUT", "PATCH", "DELETE")
                        .exposedHeaders("Authorization");
            }
        };
    }
    
    public UserController(
        UserDetailService userDetailService,
        AuthDetailService authDetailService,
        AuthMailService authMailService,
        JWTTokenUtil jwtTokenUtil
//        UserServiceBus userServiceBus
    ) {
        this.userDetailService = userDetailService;
        this.authDetailService = authDetailService;
        this.authMailService = authMailService;
        this.jwtTokenUtil = jwtTokenUtil;
//        this.userServiceBus = userServiceBus;
    }

    @GetMapping("/useers")
    public List getetes() {
        return userDetailService.getUsers();
    }
    
    @GetMapping("/users/user/{id}")
    public ResponseEntity<EntityModel<User>> getUser(@PathVariable UUID id) {
        
        EntityModel<User> foundUser = userDetailService.getUser(id);
        
        return ResponseEntity
            .ok()
            .body(foundUser);
    }
    
    @PatchMapping("/users/user/{id}")
    public ResponseEntity<EntityModel<User>> replaceUser(
        @RequestBody User newUser,
        @PathVariable UUID id,
        @RequestHeader String Authorization
    ) {
        
        authDetailService.authorizeUserRequest(id, Authorization);
        User updatedUser = userDetailService.updateUser(id, newUser);
        
        return ResponseEntity
            .ok()
            .body(EntityModel.of(updatedUser));
    }
    
    @PatchMapping("/users/user/{id}/{property}")
    public ResponseEntity<EntityModel<User>> updateUser(
        @PathVariable UUID id,
        @PathVariable String property,
        @RequestHeader String Authorization,
        @RequestBody UserPatchRequest userPatchRequest
    ) {
        
        authDetailService.authorizeUserRequest(id, Authorization);
        User updatedUser = userDetailService.updateUserProperty(id, property, userPatchRequest);
        String jwtToken = jwtTokenUtil.generateToken(JSONParser.userToJson(updatedUser));
        
        return ResponseEntity
            .ok()
            .header(AUTH_HEADER_NAME, "Bearer " + jwtToken)
            .body(EntityModel.of(updatedUser));
    }
    
    @DeleteMapping("/users/user/{id}")
    public ResponseEntity deleteUser(
        @PathVariable UUID id,
        @RequestHeader String Authorization
    ) {
       
        System.out.println("\n\n\n\n\n\n" + Authorization + "\n\n\n\n");
        authDetailService.authorizeUserRequest(id, Authorization);
        userDetailService.deleteUser(id);

        return ResponseEntity.ok().build();
    }
    
//    This section is brought from the Auth service and must authenticate users.
    @PostMapping("/users/auth/login")
    public ResponseEntity login(@RequestBody UserRequest user) {
        
        String jwtToken = authDetailService.login(user);
        
        return ResponseEntity
                .ok()
                .header(AUTH_HEADER_NAME, "Bearer " + jwtToken)
                .body(JSONParser.JSONGenerator("message", "You have logged in."));
    }
    
    @PostMapping("/users/auth/signup")
    public ResponseEntity signup(@RequestBody UserRequest user) {
        
        String jwtToken = authDetailService.signup(user);
        
        return ResponseEntity
                .ok()
                .header(AUTH_HEADER_NAME, "Bearer " + jwtToken)
                .body(JSONParser.JSONGenerator("message", "You have signed up."));
    }
    
    @GetMapping("/users/auth/logout")
    public ResponseEntity logout() {
        return ResponseEntity.ok().build();
    }
    
    @GetMapping("/users/auth/forget-password")
    public ResponseEntity forgetPassword(@RequestParam String username) {

        String passwordResetToken = authDetailService.forgetPassword(username);
        String userEmail = userDetailService.getUserByUsername(username).getEmail();
        
        try {
            this
                .authMailService
                .sendForgetPasswordEmail(username, userEmail, "Password Reset", passwordResetToken);
        } catch ( Exception e ) {
            System.out.println("Sending forget-password link to client failed.");
        }
        
        return ResponseEntity
                .ok()
                .build();
    }    
    
    @PostMapping("/users/auth/reset-password")
    public ResponseEntity resetPassword(
        @RequestBody User userAuthRequest,
        @RequestHeader(name = "Password-Reset") String passwordResetToken
    ) {
        
        String passwordResetHash = authDetailService.verifyJwtToken(passwordResetToken);
        authDetailService.verifyUserHash(userAuthRequest.getUsername(), passwordResetHash);
                
        authDetailService.resetUserPassword(
            userAuthRequest.getUsername(),
            userAuthRequest.getPassword()
        );
        
        return ResponseEntity
            .ok()
            .build();
    }
    
    
}