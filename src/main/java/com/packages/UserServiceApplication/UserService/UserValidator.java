package com.packages.UserServiceApplication.UserService;

import com.packages.UserServiceApplication.UserServiceExceptions.BadRequestException;
import java.util.regex.Pattern;

public class UserValidator {    

    public static boolean validatePhoneNumber(String phoneNumber) {
        return Pattern.matches("[+][0-9]{8,}", phoneNumber);
    }
    
    public static boolean validateName(String name) {
        return name.length() > 4 && name.length() < 20;
    }
    
    public static boolean validatePassword(String password) {
        return Pattern.matches("[a-zA-Z0-9]*[#$%^ &*@!]+[a-zA-Z0-9]*", password);
    }
    
    public static boolean validateEmail(String email) {
        return Pattern.matches("[a-zA-Z0-9]{4,}@(gmail|yahoo|outlook|icloud).(com|net|io|org|co)", email);
    }
    
    public static boolean validateCountry(String country) {
        return country.length() > 2 && country.length() < 20;
    }
    
    public static boolean validateAge(int age) {
        return age > 15 && age < 80;
    }
    
    public static boolean validateUserForSave(User user) {
        boolean[] hasAllFields = new boolean[2];
        
        if ( user.getUsername() != null && user.getUsername().length() > 4 )
            hasAllFields[0] = true;
        else
            throw new BadRequestException("Valid username must provided.");
        if (
            user.getPassword() != null &&
            user.getPassword().length() > 8 &&
            validatePassword(user.getPassword())
        )
            hasAllFields[1] = true;
        else
            throw new BadRequestException("Valid and secure password must provided.");

        boolean finalState = true;
        
        for ( boolean state : hasAllFields )
            if ( !state )
                finalState = false;
        
        return finalState;
    }
    
    
    public static boolean validateUserForUpdate(User user) {
        boolean[] hasAllFields = new boolean[6];
        
        if ( validateName(user.getUsername()) )
            hasAllFields[0] = true;
        else
            throw new BadRequestException("Valid username must provided.");
        if ( validateName(user.getFirstName()) )
            hasAllFields[1] = true;
        else
            throw new BadRequestException("Valid first name must provided.");
        if ( validateName(user.getLastName()) )
            hasAllFields[2] = true;
        else
            throw new BadRequestException("Valid last name must provided.");
        if ( validateEmail(user.getEmail()) )
            hasAllFields[3] = true;
        else
            throw new BadRequestException("Valid email must provided.");
        if ( validatePassword(user.getPassword()) )
            hasAllFields[4] = true;
        else
            throw new BadRequestException("Valid password must provided.");
        if ( validatePhoneNumber(user.getPhoneNumber()) )
            hasAllFields[5] = true;
        else
            throw new BadRequestException("Valid phone number must be provided.");

        boolean finalState = true;
        
        for ( boolean state : hasAllFields )
            if ( !state )
                finalState = false;
        
        return finalState;
    }
    
    
}
