package com.packages.UserServiceApplication.UserService;

public class UserPatchRequest {

    private String value;
    
    public UserPatchRequest() {}

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    
}
