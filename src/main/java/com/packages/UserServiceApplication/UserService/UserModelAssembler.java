package com.packages.UserServiceApplication.UserService;

import java.util.Random;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.hateoas.server.reactive.WebFluxLinkBuilder;
import org.springframework.stereotype.Component;

@Component
public class UserModelAssembler implements RepresentationModelAssembler<User, EntityModel<User>> {
    
    @Override
    public EntityModel<User> toModel(User user) {
        return EntityModel.of(
            user,
            WebMvcLinkBuilder.linkTo(WebFluxLinkBuilder.methodOn(UserController.class)
            .getUser(user.getId())).withSelfRel(),
            WebMvcLinkBuilder.linkTo(WebFluxLinkBuilder.methodOn(UserController.class))
            .withRel("users")
        );
    }
    
    public User excludePassword(User user) {
        user.setPassword(null);
        return user;
    }
    
    public User excludeCredentials(User user) {
        user.setPassword(null);
        user.setPasswordResetToken(null);
        user.setPasswordResetTokenExpiration(null);
        return user;
    }
    
    public byte[] randomBytes(int length) {
        byte[] bytes = new byte[length];
        new Random().nextBytes(bytes);
        
        return bytes;
    }
    
    public User userRequestToUser(UserRequest user) {
        User newUser = new User();
        newUser.setUsername(user.getUsername());
        newUser.setEmail(user.getEmail());
        newUser.setPassword(user.getPassword());
        
        return newUser;
    }
    
}
