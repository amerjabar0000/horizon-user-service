package com.packages.UserServiceApplication.UserService;

import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public class JSONParser {
    
    public static JSONObject userToJson(User user) {
        JSONObject object = new JSONObject();
        object.put("id", user.getId());
        object.put("username", user.getUsername());
        object.put("email", user.getEmail());
        object.put("age", user.getAge());
        object.put("country", user.getCountry());
        object.put("firstName", user.getFirstName());
        object.put("lastName", user.getLastName());
        object.put("phoneNumber", user.getPhoneNumber());
        
        return object;
    }
    
    public static Map JSONGenerator(String key, String value) {
        HashMap object = new HashMap();
        object.put(key, value);
        return object;
    }
    
    
    
}
