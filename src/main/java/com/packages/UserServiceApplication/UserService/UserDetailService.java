package com.packages.UserServiceApplication.UserService;

import com.packages.UserServiceApplication.UserServiceExceptions.ConflictException;
import com.packages.UserServiceApplication.UserServiceExceptions.PayloadNotAcceptedException;
import com.packages.UserServiceApplication.UserServiceExceptions.UserNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.hateoas.EntityModel;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class UserDetailService implements UserDetailsService {
    
    private final UserRepository userRepository;
    private final UserModelAssembler userAssembler;

    public UserDetailService(
        UserRepository userRepository,
        UserModelAssembler userAssembler
    ) {
        this.userRepository = userRepository;
        this.userAssembler = userAssembler;
    }
    
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return null;
    }
    
    public List<User> getUsers() {
        return userRepository.findAll();
    }
    
    public EntityModel<User> getUser(UUID id) {
        Optional<User> foundUser = userRepository.findById(id);
        if ( !foundUser.isPresent() )
            throw new UserNotFoundException(id);
        
        return userAssembler.toModel(userAssembler.excludePassword(foundUser.get()));
    }
    
    public User getUserByUsername(String username) {
        Optional<User> user = userRepository.findByUsername(username);
        return user.get();
    }
    
    public User updateUser(UUID id, User newUser) {
        
        if ( !userRepository.findById(id).isPresent() )
            throw new UserNotFoundException(id);
        if ( !UserValidator.validateUserForUpdate(newUser) )
            throw new PayloadNotAcceptedException();
        
        return userRepository
            .findById(id)
            .map(foundUser -> {
                foundUser.setUsername(newUser.getUsername());
                foundUser.setFirstName(newUser.getFirstName());
                foundUser.setLastName(newUser.getLastName());
                foundUser.setEmail(newUser.getEmail());
                foundUser.setPassword(newUser.getPassword());
                foundUser.setPhoneNumber(newUser.getPhoneNumber());
                foundUser.setAge(newUser.getAge());
                foundUser.setCountry(newUser.getCountry());
                return userRepository.save(foundUser);
            }).get();
    }
    
    public User updateUserProperty(UUID id, String property, UserPatchRequest userPatchRequest) {
        
        if ( !userRepository.findById(id).isPresent() )
            throw new UserNotFoundException(id);
                
        Optional<User> user = userRepository.findById(id);
        updateUserProperty(property, userPatchRequest.getValue(), user.get());
        
        try {
            userRepository.save(user.get());
            return user.get();
        } catch ( Exception e ) {
            throw new ConflictException("A conflict occured while updating user information");
        }
    }
    
    public void deleteUser(UUID id) {
        if ( !userRepository.findById(id).isPresent() )
            throw new UserNotFoundException(id);
            
        try {
            userRepository.deleteById(id);
        } catch ( Exception e ) {
            throw new ConflictException("An error occured while deleting user.");
        }
    }

    public void updateUserProperty(
            String property, 
            String value, 
            User user
    ) {
        
        switch ( property ) {
            case "email":
                if ( !UserValidator.validateEmail(value) )
                    throw new PayloadNotAcceptedException("please provide a valid email.");
                user.setEmail(value);
                break;
            case "age":
                if ( !UserValidator.validateAge(Integer.parseInt(value)) )
                    throw new PayloadNotAcceptedException("please provide a valid age number.");
                user.setAge(Integer.parseInt(value));
                break;
            case "country":
                if ( !UserValidator.validateCountry(value) )
                    throw new PayloadNotAcceptedException("please provide a valid country name.");
                user.setCountry(value);
                break;
            case "firstName":
                if ( !UserValidator.validateName(value) )
                    throw new PayloadNotAcceptedException("please provide a valid first name.");
                user.setFirstName(value);
                break;
            case "lastName":
                if ( !UserValidator.validateName(value) )
                    throw new PayloadNotAcceptedException("please provide a valid last name.");
                user.setLastName(value);
                break;
            case "phoneNumber":
                if ( !UserValidator.validatePhoneNumber(value) )
                    throw new PayloadNotAcceptedException("please provide a valid phone number.");
                user.setPhoneNumber(value);
                break;
            case "username":
                if ( !UserValidator.validateName(value) )
                    throw new PayloadNotAcceptedException("please provide a valid username.");
                user.setUsername(value);
                break;
        }
        
        userRepository.save(user);
    }
    
}


