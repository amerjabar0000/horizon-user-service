package com.packages.UserServiceApplication.UserService;

import java.util.Date;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name = "Users")
@Table(name = "users")
public class User {

    @Column(name = "id", unique = true, nullable = false)
    private @Id @GeneratedValue UUID id;

    @Column(name = "username", nullable = false, unique = true)
    private String username;

    @Column(name = "first_name", nullable = true)
    private String firstName;

    @Column(name = "last_name", nullable = true)
    private String lastName;
    
    @Column(name = "email", unique = true, nullable = true)
    private String email;
    
    @Column(name = "password", nullable = false)
    private String password;
    
    @Column(name = "country", nullable = true)
    private String country;
    
    @Column(name = "phone_number", unique = true, nullable = true)
    private String phoneNumber;
    
    @Column(name = "age", nullable = true)
    private int age;

    @Column(name = "password_reset_token", unique = false, nullable = true, columnDefinition = "TEXT")
    private String passwordResetToken;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "password_reset_token_expiration", unique = false, nullable = true, columnDefinition = "DATE")
    private Date passwordResetTokenExpiration;

    public User() {}
    
    public User(UUID id, String username, String firstName, String lastName, String email, String password, String country, String phoneNumber, int age, String passwordResetToken, Date passwordResetTokenExpiration) {
        this.id = id;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.country = country;
        this.phoneNumber = phoneNumber;
        this.age = age;
        this.passwordResetToken = passwordResetToken;
        this.passwordResetTokenExpiration = passwordResetTokenExpiration;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setPasswordResetToken(String passwordResetToken) {
        this.passwordResetToken = passwordResetToken;
    }

    public void setPasswordResetTokenExpiration(Date passwordResetTokenExpiration) {
        this.passwordResetTokenExpiration = passwordResetTokenExpiration;
    }
    
    public UUID getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getCountry() {
        return country;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public int getAge() {
        return age;
    }

    public String getPasswordResetToken() {
        return passwordResetToken;
    }

    public Date getPasswordResetTokenExpiration() {
        return passwordResetTokenExpiration;
    }

    
    
}
