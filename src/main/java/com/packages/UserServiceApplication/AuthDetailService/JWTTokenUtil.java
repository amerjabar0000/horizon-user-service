package com.packages.UserServiceApplication.AuthDetailService;

import io.jsonwebtoken.Jwts;
import org.springframework.stereotype.Component;
import java.util.Date;
import org.json.JSONObject;
import static com.packages.UserServiceApplication.AuthDetailService.Constants.*;
import com.packages.UserServiceApplication.UserServiceExceptions.BadRequestException;
import io.jsonwebtoken.JwtHandler;
import io.jsonwebtoken.JwtHandlerAdapter;

@Component
public class JWTTokenUtil {
    
    public String generateToken(JSONObject payload) {
    
        String token = Jwts
                .builder()
                .signWith(key)
                .setAudience("user")
                .setIssuer("auth service")
                .setIssuedAt(new Date())
                .setExpiration(new Date(new Date().getTime() + SESSION_EXPIRATION_DATE))
                .setSubject(payload.toString())
                .compact();
        
        return token;
    }
    
    public String generatePasswordResetToken(JSONObject payload) {
        
        String token = Jwts
                .builder()
                .signWith(key)
                .setAudience("user")
                .setIssuer("auth service")
                .setIssuedAt(new Date())
                .setExpiration(new Date(new Date().getTime() + PASSWORD_RESET_EXPIRATION_DATE))
                .setSubject(payload.toString())
                .compact();
        
        return token;
    }

    public JSONObject verifyTokenAsJSON(String jwtToken) {
        JSONObject verifiedToken;
        
        try {
            verifiedToken = new JSONObject(Jwts
                .parserBuilder()
                .setSigningKey(key)
                .build()
                .parse(jwtToken)
                .getBody()
            );
        
        } catch ( Exception e ) {
            throw new BadRequestException("Cannot verify user token.");
        }
        
        return verifiedToken;
    }
    
}


