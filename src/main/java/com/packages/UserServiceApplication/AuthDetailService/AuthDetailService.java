package com.packages.UserServiceApplication.AuthDetailService;

import com.packages.UserServiceApplication.UserService.JSONParser;
import com.packages.UserServiceApplication.UserService.User;
import com.packages.UserServiceApplication.UserService.UserRepository;
import com.packages.UserServiceApplication.UserService.UserModelAssembler;
import com.packages.UserServiceApplication.UserService.UserRequest;
import com.packages.UserServiceApplication.UserService.UserValidator;
import com.packages.UserServiceApplication.UserServiceExceptions.BadRequestException;
import com.packages.UserServiceApplication.UserServiceExceptions.PayloadNotAcceptedException;
import com.packages.UserServiceApplication.UserServiceExceptions.UserNotFoundException;
import com.packages.UserServiceApplication.UserServiceExceptions.WrongCredentialsException;
import com.packages.UserServiceApplication.UserServiceSecurity.PasswordEncoder;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

@Service
public class AuthDetailService {
    
    private final UserRepository userRepository;
    private final JWTTokenUtil jwtTokenUtil;
    private final UserModelAssembler userAssembler;
    private final PasswordEncoder passwordEncoder;
    
    public AuthDetailService(
        UserRepository userRepository,
        JWTTokenUtil jwtTokenUtil,
        UserModelAssembler userAssembler,
        PasswordEncoder passwordEncoder
    ) {
        this.userRepository = userRepository;
        this.jwtTokenUtil = jwtTokenUtil;
        this.userAssembler = userAssembler;
        this.passwordEncoder = passwordEncoder;
    }
    
    public String login(UserRequest user) {
        
        Optional<User> foundUser;
        
        String username = user.getUsername();
        
        foundUser = userRepository.findByUsername(username);
        if ( !foundUser.isPresent() )
            throw new UserNotFoundException("Wrong credentials.");
        
        if ( !passwordEncoder.matches(user.getPassword(), foundUser.get().getPassword()) )
            throw new WrongCredentialsException();
        
        String jwtToken = jwtTokenUtil.generateToken(JSONParser.userToJson(foundUser.get()));
        return jwtToken;
    }
    
    public String signup(UserRequest userObject) {
        
        if ( !UserValidator.validateName(userObject.getUsername()) )
            throw new PayloadNotAcceptedException("Username length must be longer than 4 characters.");
        if ( !UserValidator.validateEmail(userObject.getEmail()) )
            throw new PayloadNotAcceptedException("Valid email address must be provided.");
        if ( !UserValidator.validatePassword(userObject.getPassword()) )
            throw new PayloadNotAcceptedException("Provided password is weak.");

        User user;
        try {
            user = userAssembler.userRequestToUser(userObject);
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            userRepository.save(user);
        } catch ( Exception e ) {
            throw new BadRequestException("User with chosen username or email already exists.");
        }
        
        String jwtToken;
        try {
            jwtToken = jwtTokenUtil.generateToken(JSONParser.userToJson(user));
        } catch ( Exception e ) {
            throw new BadRequestException("An error occured while signing up.");
        }
        
        return jwtToken;
    }
    
    public String forgetPassword(String username) {
        
        String resetToken = this.getUsersPasswordResetToken(username);
        JSONObject resetTokenObject = new JSONObject();
        resetTokenObject.put("passwordResetToken", resetToken);
        String signedResetToken = jwtTokenUtil.generatePasswordResetToken(resetTokenObject);
        
        return signedResetToken;
    }
    
    public String verifyJwtToken(String passwordResetToken) {
        
        JSONObject verifiedPasswordResetToken = jwtTokenUtil.verifyTokenAsJSON(passwordResetToken);
        JSONObject passwordResetTokenSubject = new JSONObject(verifiedPasswordResetToken.get("subject").toString());
        String passwordResetHash = passwordResetTokenSubject.getString("passwordResetToken");
        
        return passwordResetHash;
    }
    
    public void verifyUserHash(String username, String passwordResetHash) {
        
        Optional<User> user = userRepository.findByUsername(username);
        
        if ( !user.isPresent() )
            throw new UserNotFoundException("Could not find requested user.");
        if ( !user.get().getPasswordResetToken().equals(passwordResetHash) )
            throw new WrongCredentialsException("Invalid password reset token.");
    }
    
    public void resetUserPassword(String username, String newPassword) {

        if ( !UserValidator.validatePassword(newPassword) )
            throw new PayloadNotAcceptedException("Provided password is weak.");
        
        String newHashedPassword = passwordEncoder.encode(newPassword);
        try {
            Optional<User> user = userRepository.findByUsername(username);
            user.get().setPassword(newHashedPassword);
            userRepository.save(user.get());
        } catch ( Exception e ) {
            throw new BadRequestException();
        }
        this.setUsersPasswordResetToken(username, "", null);
    }
    
    
    // Internal helper methods.
    public String getUsersPasswordResetToken(String username) {
        Optional<User> foundUser = userRepository.findByUsername(username);
        if ( !foundUser.isPresent() )
            throw new UserNotFoundException("User does not exist.");
        
        String passwordResetToken = "";
        
        if (
            foundUser.get().getPasswordResetToken() != null &&
            foundUser.get().getPasswordResetTokenExpiration() != null &&
            new Date(foundUser.get().getPasswordResetTokenExpiration().getTime()).getTime() > (new Date()).getTime() - 3600
        )
            passwordResetToken = foundUser.get().getPasswordResetToken();
        else {
            passwordResetToken = passwordEncoder.encode(
                foundUser.get().getId() + "|" +
                foundUser.get().getEmail() + "|" +
                foundUser.get().getPassword()
            );
            
            try {
                Optional<User> user = userRepository.findByUsername(username);
                user.get().setPasswordResetToken(passwordResetToken);
                user.get().setPasswordResetTokenExpiration(new Date(new Date().getTime() + 3600));
                userRepository.save(user.get());
            } catch ( Exception e ) {}
        }
        
        return passwordResetToken;
    }
    
    public void setUsersPasswordResetToken(
        String username,
        String newPasswordResetToken,
        Date newPasswordResetTokenExpiration
    ) {
        try {
            Optional<User> user = userRepository.findByUsername(username);
            user.get().setPasswordResetToken(newPasswordResetToken);
            user.get().setPasswordResetTokenExpiration(newPasswordResetTokenExpiration);
            userRepository.save(user.get());
        } catch ( Exception e ) {
            throw new BadRequestException();
        }
    }
    
    // JWT authorization helper methods.
    public void authorizeUserRequest(
        UUID id, String authorization
    ) {
        try {
            String token = authorization.split(" ")[1];
            JSONObject userJsonObject = jwtTokenUtil.verifyTokenAsJSON(token);
            JSONObject jwtSubjectMap = new JSONObject(userJsonObject.getString("subject"));

            if ( !jwtSubjectMap.getString("id").equals(id.toString()) )
                throw new WrongCredentialsException("Invalid bearer token was provided.");
        } catch ( Exception e ) {
            throw new WrongCredentialsException("Invalid bearer token was provided.");
        }        
    }    
}
