package com.packages.UserServiceApplication.AuthDetailService;

import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.io.Decoders;
import java.security.Key;

public class Constants {
    
    public static final String AUTH_HEADER_NAME = "Authorization";
    public static final String KEY = "753567aece6994d629bd8373070dc29721a66617cbec31aec5a793e8652a687d";
    public static final byte[] DECODER = Decoders.BASE64.decode(KEY);
    
    public static final Key key = Keys.hmacShaKeyFor(DECODER);
    public static final Long SESSION_EXPIRATION_DATE = 1000*60*60l;
    public static final Long PASSWORD_RESET_EXPIRATION_DATE = 1000*60*60l;
    
}
