package com.packages.UserServiceApplication.UserServiceExceptions;

import java.util.UUID;

public class UserNotFoundException extends RuntimeException {
    
    public UserNotFoundException(UUID id) {
        super("Could not find user with id: " + id);
    } 
    
    public UserNotFoundException(String message) {
        super(message);
    }
    
}
