package com.packages.UserServiceApplication.UserServiceExceptions;

public class ConflictException extends RuntimeException {
    
    public ConflictException() {
        super("A conflict occured");
    }
    
    public ConflictException(String message) {
        super(message);
    }
    
}
