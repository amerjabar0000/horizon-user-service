package com.packages.UserServiceApplication.UserServiceExceptions;

public class PayloadNotAcceptedException extends RuntimeException {
    public PayloadNotAcceptedException() {
        super("Payload was not accepted due to missing fields or bad input!");
    }
    
    public PayloadNotAcceptedException(String message) {
        super(message);
    }
}
