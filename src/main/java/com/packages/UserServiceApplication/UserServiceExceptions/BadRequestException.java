package com.packages.UserServiceApplication.UserServiceExceptions;

public class BadRequestException extends RuntimeException {
    public BadRequestException() {
        super("Bad request. An error occred.");
    }
    
    public BadRequestException(String message) {
        super(message);
    }
}
