package com.packages.UserServiceApplication.UserServiceResponses;

import com.packages.UserServiceApplication.UserServiceExceptions.WrongCredentialsException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class WrongCredentialsResponse {
    
    @ResponseBody
    @ExceptionHandler(WrongCredentialsException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public String wrongCredentialsHandler(WrongCredentialsException errorMessage) {
        return errorMessage.getMessage();
    }
    
}
