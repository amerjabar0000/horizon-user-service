package com.packages.UserServiceApplication.UserServiceResponses;

import com.packages.UserServiceApplication.UserServiceExceptions.ConflictException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ConflictResponse {
    
    @ResponseBody
    @ExceptionHandler(ConflictException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public String conflictHandler(ConflictException errorMessage) {
        return errorMessage.getMessage();
    }
    
}
